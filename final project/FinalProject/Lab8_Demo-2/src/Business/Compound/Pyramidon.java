/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Compound;

import Business.Compound.ChemicalCompound;

/**
 *
 * @author CAC
 */
public class Pyramidon extends ChemicalCompound{
    private String name;
    private String Formula;
    private double price;
    private ChemicalCompound.ChemicalType chemicalType;


      public Pyramidon(Business.Compound.ChemicalCompound.ChemicalType  type, String name, String Formula, double price) {
        
          this.name=name;
          this.Formula=Formula;
          this.chemicalType=type;
          this.price=price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFormula() {
        return Formula;
    }

    public void setFormula(String Formula) {
        this.Formula = Formula;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public ChemicalType getChemicalType() {
        return chemicalType;
    }

    public void setChemicalType(ChemicalType chemicalType) {
        this.chemicalType = chemicalType;
    }
   
       
    
}
