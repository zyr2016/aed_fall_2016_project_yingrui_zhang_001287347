/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Compound;

/**
 *
 * @author CAC
 */
public class ChemicalCompound {

 
    public enum ChemicalType{
        Antinflammatory ("Antinflammatory "),Antipyretic  ("Antipyretic  "),Cardiac  ("Cardiac  ");
        private String name;
        
        private ChemicalType(String name){
            this.name=name;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
        
        
    
    }
}
