/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Enterprise;

import java.util.ArrayList;

/**
 *
 * @author yyneu
 */
public class EnterpriseDirectory {
              
   private ArrayList<Enterprise> enterpriseList;    

    public ArrayList<Enterprise> getEnterpriseList() {
        return enterpriseList;
    }

    public void setEnterpriseList(ArrayList<Enterprise> enterpriseList) {
        this.enterpriseList = enterpriseList;
    }
   

   public EnterpriseDirectory(){
       enterpriseList= new ArrayList();
   }
 
    public Enterprise createAndAddEnterprise(String name, Enterprise.EnterpriseType type)
    {
        Enterprise enterprise = null;
        if(type == Enterprise.EnterpriseType.Hospital)
        {
            enterprise= new HospitalEnterprise(name);
            enterpriseList.add(enterprise);
        }
        ///////////////////////////////////////////////////
        
        
            if(type == Enterprise.EnterpriseType.ChemistryFactory)
        {
            enterprise= new ChemistryFactoryEnterprise(name);
            enterpriseList.add(enterprise);
        }
            ////////////////////////////////////////////////
            if(type == Enterprise.EnterpriseType.ClinicalTrails)
        {
            enterprise= new ClinicalTrialsEnterprise(name);
            enterpriseList.add(enterprise);
        }
            ////////////////////////////////////
                if(type == Enterprise.EnterpriseType.Healthcare)
        {
            enterprise= new HealthcareEnterprise(name);
            enterpriseList.add(enterprise);
        }    
                if(type == Enterprise.EnterpriseType.Marketing)
        {
            enterprise= new MarketingEnterprise(name);
            enterpriseList.add(enterprise);
        }    
                if(type == Enterprise.EnterpriseType.ResearchDevelopment)
        {
            enterprise= new ResearchDevelopmentEnterprise(name);
            enterpriseList.add(enterprise);
        }  
        return enterprise;
    }
    
    //Create Enterprise
    
}
