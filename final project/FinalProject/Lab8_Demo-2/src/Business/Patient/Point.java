/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patient;

import Business.Disease.Disease;
import Business.Disease.DiseaseHistory;

/**
 *
 * @author yyneu
 */
public class Point {
    private int agePoint;
    private int smokePoint;
    private int systolicPoint;
    private int diabetesPoint;
    private int diseaseHistoryPoint;
    
    private int totalPoint;
    private String risk;

    //calculate age point according to the age and gender 
    public int agePoint(int age, String gender) {
        if (gender.equals("male")) {
            if (age < 20) {
                agePoint = -12;
            } else if (age >= 20 & age <= 34) {
                agePoint = -9;
            } else if (age >= 35 & age <= 39) {
                agePoint = -4;
            } else if (age >= 40 & age <= 44) {
                agePoint = 0;
            } else if (age >= 45 & age <= 49) {
                agePoint = 3;
            } else if (age >= 50 & age <= 54) {
                agePoint = 6;
            } else if (age >= 55 & age <= 59) {
                agePoint = 8;
            } else if (age >= 60 & age <= 64) {
                agePoint = 10;
            } else if (age >= 65 & age <= 69) {
                agePoint = 11;
            } else if (age >= 70 & age <= 74) {
                agePoint = 12;
            } else if (age >= 75 & age <= 79) {
                agePoint = 13;
            }
        } else if (gender.equals("female")) {
            if (age < 20) {
                agePoint = -10;
            } else if (age >= 20 & age <= 34) {
                agePoint = -7;
            } else if (age >= 35 & age <= 39) {
                agePoint = -3;
            } else if (age >= 40 & age <= 44) {
                agePoint = 0;
            } else if (age >= 45 & age <= 49) {
                agePoint = 3;
            } else if (age >= 50 & age <= 54) {
                agePoint = 6;
            } else if (age >= 55 & age <= 59) {
                agePoint = 8;
            } else if (age >= 60 & age <= 64) {
                agePoint = 10;
            } else if (age >= 65 & age <= 69) {
                agePoint = 12;
            } else if (age >= 70 & age <= 74) {
                agePoint = 14;
            } else if (age >= 75 & age <= 79) {
                agePoint = 16;
            }
        }
        return agePoint;
    }
    


    //calculate smokePoint according to the whether smoke ,age and gender
    public int smokePoint(boolean smoke, int age, String gender) {
        if (gender.equals("male")) {
            if (smoke == true) {
                if (age < 20) {
                    smokePoint = 12;
                } else if (age >= 20 & age <= 39) {
                    smokePoint = 8;
                } else if (age >= 40 & age <= 49) {
                    smokePoint = 5;
                } else if (age >= 50 & age <= 59) {
                    smokePoint = 3;
                } else if (age >= 60 & age <= 69) {
                    smokePoint = 1;
                } else if (age >= 70 & age <= 79) {
                    smokePoint = 1;
                }
            } else {
                smokePoint = 0;
            }
        } else if (gender.equals("female")) {
            if (smoke == true) {
                if (age < 20) {
                    smokePoint = 13;
                } else if (age >= 20 & age <= 39) {
                    smokePoint = 9;
                } else if (age >= 40 & age <= 49) {
                    smokePoint = 7;
                } else if (age >= 50 & age <= 59) {
                    smokePoint = 4;
                } else if (age >= 60 & age <= 69) {
                    smokePoint = 2;
                } else if (age >= 70 & age <= 79) {
                    smokePoint = 1;
                }
            } else {
                smokePoint = 0;
            }
        }
        return smokePoint;
    }

    //calculate diabete point according to whether diabetes and age
    public int diabetesPoint(boolean diabetes, int age) {
        if (diabetes = true) {
            if (age <= 39) {
                diabetesPoint = 3;
            } else if (age >= 40 & age <= 59) {
                diabetesPoint = 2;
            } else if (age >= 60) {
                diabetesPoint = 1;
            }
        } else if (diabetes = false) {
            diabetesPoint = 0;
        }
        return diabetesPoint;
    }

    public int systolicPoint(double systolicBloodPressure, String gender) {
        if (gender.equals("male")) {
        
                if (systolicBloodPressure < 120) {
                    systolicPoint = 0;
                } else if (systolicBloodPressure >= 120 & systolicBloodPressure < 130) {
                    systolicPoint = 0;
                } else if (systolicBloodPressure >= 130 & systolicBloodPressure < 140) {
                    systolicPoint = 1;
                } else if (systolicBloodPressure >= 140 & systolicBloodPressure < 160) {
                    systolicPoint = 1;
                } else if (systolicBloodPressure >= 160) {
                    systolicPoint = 2;
                }
            
        } else if (gender.equals("female")) {
        
                if (systolicBloodPressure < 120) {
                    systolicPoint = 0;
                } else if (systolicBloodPressure >= 120 & systolicBloodPressure < 130) {
                    systolicPoint = 1;
                } else if (systolicBloodPressure >= 130 & systolicBloodPressure < 140) {
                    systolicPoint = 2;
                } else if (systolicBloodPressure >= 140 & systolicBloodPressure < 160) {
                    systolicPoint = 3;
                } else if (systolicBloodPressure >= 160) {
                    systolicPoint = 4;
                }
            
        }
        return systolicPoint;
    }
    //calculate diseaseHistoryPoint
    public int diseaseHistoryPoint(DiseaseHistory diseaseHistory, int age){
     
       double diseaseAllPoint=0;
    for(Disease disease:diseaseHistory.getDiseaseHistory()){
        double diseasePoint=0;
    if (age <= 39){
     if(disease.isIsTreated()==true){
         if(disease.getTreatedTime()<=5){
             diseasePoint=0;
         }
         else diseasePoint=0.8*(disease.getTreatedTime()-5);     
     }
     else if(disease.isIsTreated()==false){
       if(disease.getTreatedTime()<=5){
             diseasePoint=1;
         }  
       else if(disease.getTreatedTime()<=15&&disease.getTreatedTime()>5){
           diseasePoint=1+0.7*(disease.getTreatedTime()-5);
       } 
       else diseasePoint=8+0.9*(disease.getTreatedTime()-5);
     }
     }
    else if (age>39&&age<60) {
        if(disease.isIsTreated()==true){
         if(disease.getTreatedTime()<=5){
             diseasePoint=-1;
         }
         else diseasePoint=0.6*(disease.getTreatedTime()-5);     
     }
     else if(disease.isIsTreated()==false){
       if(disease.getTreatedTime()<=5){
             diseasePoint=1;
         }  
       else if(disease.getTreatedTime()<=15&&disease.getTreatedTime()>5){
           diseasePoint=1+0.5*(disease.getTreatedTime()-5);
       } 
       else diseasePoint=6+0.7*(disease.getTreatedTime()-5);
     }   
    }
    else if(age>=60){
        if(disease.isIsTreated()==true){
         if(disease.getTreatedTime()<=5){
             diseasePoint=-1;
         }
         else diseasePoint=0.5*(disease.getTreatedTime()-5);     
     }
     else if(disease.isIsTreated()==false){
       if(disease.getTreatedTime()<=5){
             diseasePoint=1;
         }  
       else if(disease.getTreatedTime()<=15&&disease.getTreatedTime()>5){
           diseasePoint=1+0.4*(disease.getTreatedTime()-5);
       } 
       else diseasePoint=5+0.6*(disease.getTreatedTime()-5);
     }   
    }
    
    diseaseAllPoint=diseaseAllPoint+diseasePoint;
     }
       
     diseaseHistoryPoint= (int)Math.round(diseaseAllPoint);
     return diseaseHistoryPoint;
      
   }
//calculate total point

    public int totalPoint(int agePoint, int smokePoint, int systolicPoint, int diabetesPoint,int diseaseHistoryPoint) {
        totalPoint = agePoint + smokePoint + systolicPoint + diabetesPoint+ diseaseHistoryPoint;
        return totalPoint;
    }

    public String report(int totalPoint) {
        if (totalPoint >= 17) {
            risk = "high";
        } else if (totalPoint >= 12 & totalPoint < 17) {
            risk = "medium";
        } else if (totalPoint >= 7 & totalPoint < 12) {
            risk = "low";
        } else if (totalPoint < 7) {
            risk = "very health";
        }
        return risk;
    }
//getter&setter

    public int getAgePoint() {
        return agePoint;
    }

    public void setAgePoint(int agePoint) {
        this.agePoint = agePoint;
    }


    public int getSmokePoint() {
        return smokePoint;
    }

    public void setSmokePoint(int smokePoint) {
        this.smokePoint = smokePoint;
    }

    public int getSystolicPoint() {
        return systolicPoint;
    }

    public void setSystolicPoint(int systolicPoint) {
        this.systolicPoint = systolicPoint;
    }

    public int getTotalPoint() {
        return totalPoint;
    }

    public void setTotalPoint(int totalPoint) {
        this.totalPoint = totalPoint;
    }

    public String getRisk() {
        return risk;
    }

    public void setRisk(String risk) {
        this.risk = risk;
    }

    public int getDiabetesPoint() {
        return diabetesPoint;
    }

    public void setDiabetesPoint(int diabetesPoint) {
        this.diabetesPoint = diabetesPoint;
    }

    public int getDiseaseHistoryPoint() {
        return diseaseHistoryPoint;
    }

    public void setDiseaseHistoryPoint(int diseaseHistoryPoint) {
        this.diseaseHistoryPoint = diseaseHistoryPoint;
    }

}
