/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patient;

import Business.Disease.Disease;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author yyneu
 */
public class Report {
    
   
    
   public City iniWhole()throws ParseException
   {
      InitialData ww=new InitialData();
       City city = ww.iniAll(); 
       
       int agePoint;
       int smokePoint;
       int systolicPoint;
       int diabetesPoint;
       int diseaseHistoryPoint;
       int totalPoint;
       
       String risk;
       ArrayList<String> diseaseName;
       ArrayList<String> drugName;
       ArrayList<String> hospitalName;
       ArrayList<Disease> diseaseList;
       ArrayList<Person> iniParentList;
       ArrayList<Person> iniChildrenList;
       ArrayList<Person> iniGrandparentList;
       ArrayList<Person> iniPersonList;
       
       diseaseName=ww.iniDiseaseName();
       drugName=ww.iniDrugName();
       hospitalName=ww.iniHospitalName();
       diseaseList=ww.iniDisease(drugName, diseaseName, hospitalName);
       iniParentList = ww.iniParentList(ww.iniName(),diseaseList);
       iniChildrenList = ww.iniChildrenList(ww.iniName(),diseaseList);
       iniGrandparentList = ww.iniGrandparentList(ww.iniName(),diseaseList);
      
       
         ArrayList<Person> parentList = new ArrayList<Person>();
         for(Person p:iniParentList){
             Point point = new Point();
             agePoint=point.agePoint(p.getAge(), p.getGender());
             smokePoint=point.smokePoint(p.isSmoker(), p.getAge(), p.getGender());
             systolicPoint=point.systolicPoint(p.getSystolicBloodPressure(), p.getGender());
             diabetesPoint=point.diabetesPoint(p.isDiabetes(), p.getAge());
             diseaseHistoryPoint=point.diseaseHistoryPoint(p.getDiseaseHistory(), p.getAge());
             totalPoint=point.totalPoint(agePoint, smokePoint, systolicPoint, diabetesPoint, diseaseHistoryPoint);
             risk=point.report(totalPoint);
             point.setAgePoint(agePoint);
             point.setSmokePoint(smokePoint);
             point.setSystolicPoint(systolicPoint);
             point.setDiabetesPoint(diabetesPoint);
             point.setDiseaseHistoryPoint(diseaseHistoryPoint);
             point.setTotalPoint(totalPoint);
             point.setRisk(risk);//calculate different person's point
             p.setPoint(point);
             parentList.add(p);
         }
        iniParentList=parentList;
        
         ArrayList<Person> childrenList = new ArrayList<Person>();
         for(Person p:iniChildrenList){
             Point point = new Point();
             agePoint=point.agePoint(p.getAge(), p.getGender());
             smokePoint=point.smokePoint(p.isSmoker(), p.getAge(), p.getGender());
             systolicPoint=point.systolicPoint(p.getSystolicBloodPressure(), p.getGender());
             diabetesPoint=point.diabetesPoint(p.isDiabetes(), p.getAge());
             diseaseHistoryPoint=point.diseaseHistoryPoint(p.getDiseaseHistory(), p.getAge());
             totalPoint=point.totalPoint(agePoint, smokePoint, systolicPoint, diabetesPoint, diseaseHistoryPoint);
             risk=point.report(totalPoint);
             point.setAgePoint(agePoint);
             point.setSmokePoint(smokePoint);
             point.setSystolicPoint(systolicPoint);
             point.setDiabetesPoint(diabetesPoint);
             point.setDiseaseHistoryPoint(diseaseHistoryPoint);
             point.setTotalPoint(totalPoint);
             point.setRisk(risk);//calculate different person's point
             p.setPoint(point);
             childrenList.add(p);
         }
        iniChildrenList=childrenList;
        
        
         ArrayList<Person> grandparentList = new ArrayList<Person>();
         for(Person p:iniGrandparentList){
             Point point = new Point();
             agePoint=point.agePoint(p.getAge(), p.getGender());
             smokePoint=point.smokePoint(p.isSmoker(), p.getAge(), p.getGender());
             systolicPoint=point.systolicPoint(p.getSystolicBloodPressure(), p.getGender());
             diabetesPoint=point.diabetesPoint(p.isDiabetes(), p.getAge());
             diseaseHistoryPoint=point.diseaseHistoryPoint(p.getDiseaseHistory(), p.getAge());
             totalPoint=point.totalPoint(agePoint, smokePoint, systolicPoint, diabetesPoint, diseaseHistoryPoint);
             risk=point.report(totalPoint);
             point.setAgePoint(agePoint);
             point.setSmokePoint(smokePoint);
             point.setSystolicPoint(systolicPoint);
             point.setDiabetesPoint(diabetesPoint);
             point.setDiseaseHistoryPoint(diseaseHistoryPoint);
             point.setTotalPoint(totalPoint);
             point.setRisk(risk);//calculate different person's point
             p.setPoint(point);
             grandparentList.add(p);
         }
            iniGrandparentList=grandparentList;
        
         iniPersonList = ww.iniPerson(iniParentList, iniChildrenList, iniGrandparentList);
         ArrayList<Family> familyList = ww.iniFamily(iniParentList, iniChildrenList, iniGrandparentList);
         ArrayList<House>  houseList = ww.iniHouse(familyList);
         ArrayList<Community> communityList = ww.iniCommunity(houseList);
         city.setCommunityList(communityList);
         return city;
    }
   
   
  public int familyLowRiskNumber(ArrayList<Family> familyList){
                int familyNumber=0;
                         for(Family f:familyList){
                             int personI=0;
                            
                            for(Person p:f.getPersonList()){
                                if(p.getPoint().getRisk().equals("low"))
                                    personI++;
                            }
                            //calculate the low risk person number
                            if(personI==f.getPersonList().size())
                                familyNumber++;
                         }
                 return familyNumber;
  }
  
  public String peopleCommonStatus(ArrayList<Person> personList){
                        int veryHealth=0;
                        int low=0;
                        int medium=0;
                        int high=0;
                        for(Person p:personList){
                            if(p.getPoint().getRisk().equals("very health"))veryHealth++;
                            else if(p.getPoint().getRisk().equals("low"))low++;
                            else if(p.getPoint().getRisk().equals("medium"))medium++;
                            else if(p.getPoint().getRisk().equals("high"))high++; 
                        }
                        //calculate each status people member
                        int[] num = new int[4];
                        num[0]=veryHealth;
                        num[1]=low;
                        num[2]=medium;
                        num[3]=high;
                         HashMap<Integer, String> hashMap = new HashMap<Integer, String>();
                         hashMap.put(veryHealth, "very Health");
                         hashMap.put(low, "low");
                         hashMap.put(medium, "medium");
                         hashMap.put(high, "high");
                         //assign key to each status
                        int z=0;
                        for(int i=0;i<4;i++)
                            for(int j=0;j<3-i;j++){
                                if(num[j]>num[j+1])
                                   z=num[j+1];
                                num[j+1]=num[j];
                                num[j]=z;
                            }
                    return hashMap.get(num[3]);
                        
  }
  public ArrayList<String> averageHealthStatusPerCommunity(City city){
      ArrayList<String> riskList=new ArrayList<String>();
      for(Community c:city.getCommunityList())
                      {
                             int communityPoint=0;
                             int communityMemeber=0;
                         for(House h:c.getHouseList())
                             for(Family f:h.getFamilyList())
                                 for(Person p:f.getPersonList()){
                                    communityPoint=communityPoint+p.getPoint().getTotalPoint();
                                    communityMemeber++;
                                 }
                             double averagePoint=communityPoint/communityMemeber;
                             c.setRisk(c.report((int)averagePoint));
                             riskList.add(c.getRisk());
                         }
      return riskList;
    }
  public int peopleHighRiskThrityYearsOld(ArrayList<Person> personList){
      ArrayList<Person> personList1 = new ArrayList<Person>(); 
                 for(Person p:personList){
                       if(p.getAge()>30&p.getPoint().getRisk().equals("high")) personList1.add(p);
                    }
              return personList1.size();
  }

  public int femaleToMaleHighRisk(ArrayList<Person> personList){
        int numberMale=0;
                   int numberFemale=0;
                   int differ=0;
                    for(Person p:personList){
                              if(p.getGender().equals("male")&p.getPoint().getRisk().equals("high"))
                                numberMale++;
                                else if(p.getGender().equals("female")&p.getPoint().getRisk().equals("high"))
                                 numberFemale++;
                   }
                   if(numberMale>numberFemale) differ=numberMale-numberFemale;
                   else differ=numberFemale-numberMale;
                   return differ;
  }
  public String TopDiseasePeopleHave(ArrayList<Person> personList){
      
       HashMap<String,Integer> hashmap=new HashMap<String,Integer>();
      for(Person p:personList){
          for(Disease d:p.getDiseaseHistory().getDiseaseHistory()){
              if(hashmap.containsKey(d.getName())){
                  int temp=hashmap.get(d.getName());
                  hashmap.put(d.getName(), temp);
              }
              else {  
                hashmap.put(d.getName(), 1);  
            }  
          }   
      }
        Collection<Integer> count = hashmap.values();
        int maxCount = Collections.max(count);  
        String mostDisease = "";
         for (Map.Entry<String, Integer> entry : hashmap.entrySet()) {  
            if (maxCount == entry.getValue()) {  
                mostDisease = entry.getKey();  
            }  
        }
         return mostDisease;
      
  }
  public String TopUsedDrug(ArrayList<Person> personList){
      
    HashMap<String,Integer> hashmap=new HashMap<String,Integer>();
      for(Person p:personList){
          for(Disease d:p.getDiseaseHistory().getDiseaseHistory()){
              if(hashmap.containsKey(d.getDrug())){
                  int temp=hashmap.get(d.getDrug());
                  hashmap.put(d.getDrug(), temp);
              }
              else {  
                hashmap.put(d.getDrug(), 1);  
            }  
          }   
      }
        Collection<Integer> count = hashmap.values();
        int maxCount = Collections.max(count);  
        String mostDrug = "";
         for (Map.Entry<String, Integer> entry : hashmap.entrySet()) {  
            if (maxCount == entry.getValue()) {  
                mostDrug = entry.getKey();  
            }  
        }
         return mostDrug;
  }
  
  
  public int peopleHighRiskCity(ArrayList<Person> personList){
      int number=0;
                  for(Person p:personList){
                   if(p.getPoint().getRisk().equals("high"))number++;
                                 }
                  return number;
  }
  
}
