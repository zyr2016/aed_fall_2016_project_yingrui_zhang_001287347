/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patient;

import Business.Disease.Disease;
import Business.Disease.DiseaseHistory;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Random;

/**
 *
 * @author yyneu
 */
public class InitialData {
      int j=0;
   public ArrayList<String> iniName() {
        ArrayList<String> firstName = new ArrayList<String>();
        ArrayList<String> middleName = new ArrayList<String>();
        ArrayList<String> lastName = new ArrayList<String>();
        ArrayList<String> name = new ArrayList<String>();

        firstName.add("Jim");
        firstName.add("Rose");
        firstName.add("Evan");
        firstName.add("Max");
        firstName.add("John");
        firstName.add("Allen");
        firstName.add("Mary");
        firstName.add("Harry");
        firstName.add("Hana");
        firstName.add("Lisa");

        middleName.add(" .A. ");
        middleName.add(" .B. ");
        middleName.add(" .C. ");
        middleName.add(" .R. ");
        middleName.add(" .J. ");
        middleName.add(" .C. ");
        middleName.add(" .M. ");
        middleName.add(" .I. ");
        middleName.add(" .V. ");
        middleName.add(" .C. ");

        lastName.add("Black");
        lastName.add("Smith");
        lastName.add("Davids");
        lastName.add("Williams");
        lastName.add("King");
        lastName.add("Bush");
        lastName.add("Brown");
        lastName.add("Robinson");
        lastName.add("Prior");
        lastName.add("Rice");

        for (String s : firstName) {
            for (String d : middleName) {
                for (String f : lastName) {
                    String nameF = s + d + f;
                    name.add(nameF);
                }
            }
        }

        return name;

    }
     public ArrayList<String> iniDiseaseName(){
      ArrayList<String> diseaseName = new ArrayList<String>();
        diseaseName.add("indigestion");
        diseaseName.add("gastritis");
        diseaseName.add("enteritis");
        diseaseName.add("flu");
        diseaseName.add("rhinitis");
        diseaseName.add("apoplexy");
        diseaseName.add("bronchitis");
        diseaseName.add("pneumonia");
        diseaseName.add("periodontitis");
        diseaseName.add("food poisoning");
        diseaseName.add("anemia");
        diseaseName.add("varicose vein");
        diseaseName.add("appendicitis");
        diseaseName.add("conjunctivitis");
        diseaseName.add("otitis media");
        return diseaseName;
}
     public ArrayList<String> iniDrugName(){
      ArrayList<String> drugName = new ArrayList<String>();
        drugName.add("amoxycillin");
        drugName.add("amphotericin");
        drugName.add("ampicillin");
        drugName.add("aspirin");
        drugName.add("chloromycetin");
        drugName.add("codeine");
        drugName.add("cortisone");
        drugName.add("dexamethasone");
        drugName.add("digitoxin");
        drugName.add("diphenhydramine");
        drugName.add("erythromycin");
        drugName.add("ethosuximide");
        drugName.add("fleroxacin");
        drugName.add("griseofulvin");
        drugName.add("hydrocortisone");
        return drugName;
}
     public ArrayList<Element> iniALlElement(){
      Element element1=new Element();
      element1.setName("C");
      Element element2=new Element();
      element2.setName("N");
      Element element3=new Element();
      element3.setName("O");
      Element element4=new Element();
      element4.setName("S");
      Element element5=new Element();
      element5.setName("H");
       ArrayList<Element> elementList=new ArrayList<Element>();
        elementList.add(element1);
         elementList.add(element2);
         elementList.add(element3);
         elementList.add(element4);
         elementList.add(element5);
         return elementList;
     }
     
     public ArrayList<Cata> iniAllCatalog(){
         ArrayList<Cata> catalogList=new ArrayList<Cata>();
        Cata catalog1=new Cata();
        Cata catalog2=new Cata();
        Cata catalog3=new Cata();
        Cata catalog4=new Cata();
        String[] array1 ={"C","H","O"};
        String[] array2 ={"C","H","O"};
        String[] array3 ={"C","H","N","O"};
        String[] array4 ={"H","Ci"};
       
        catalog1.setCatalog(array1);
        catalog2.setCatalog(array2);
        catalog3.setCatalog(array3);
        catalog4.setCatalog(array4);
        catalogList.add(catalog1);
        catalogList.add(catalog2);
        catalogList.add(catalog3);
        catalogList.add(catalog4);
        return catalogList;
     }
   
     public ArrayList<Compound> iniAllCompound(ArrayList<Cata> catalogList){
         ArrayList<Compound> compoundList=new ArrayList<Compound>();
         Compound compound1=new Compound();
         String name1="propylene glycol";
         String description1="Colorless viscous liquid";
         String molecularFormula1="C3N6O2";
         String molecularStructure1="CNOC2N5O";
         compound1.setName(name1);
         compound1.setDescription(description1);
         compound1.setMolecularFormula(molecularFormula1);
         compound1.setMolecularStructure(molecularStructure1);
         compound1.setType(catalogList.get(0));
         Compound compound2=new Compound();
         String name2="ethanol";
         String description2="Flammable, volatile, colorless transparent liquid";
         String molecularFormula2="C2H6O";
         String molecularStructure2="CNCH5O";
         compound2.setName(name2);
         compound2.setDescription(description2);
         compound2.setMolecularFormula(molecularFormula2);
         compound2.setMolecularStructure(molecularStructure2);
         compound2.setType(catalogList.get(1));
         Compound compound3=new Compound();
         String name3="ClarithromycinDispersible Tablets";
         String description3="It is mainly used for upper and lower respiratory tract infection caused by sensitive bacteria";
         String molecularFormula3="C38H69NO13";
         String molecularStructure3="C2H2NO2C36H67O11";
         compound3.setName(name3);
         compound3.setDescription(description3);
         compound3.setMolecularFormula(molecularFormula3);
         compound3.setMolecularStructure(molecularStructure3);
         compound3.setType(catalogList.get(2));
         Compound compound4=new Compound();
         String name4="hydrochloric";
         String description4="Have a strong pungent odor, has high corrosion resistance";
         String molecularFormula4="HCI";
         String molecularStructure4="HCI";
         compound4.setName(name4);
         compound4.setDescription(description4);
         compound4.setMolecularFormula(molecularFormula4);
         compound4.setMolecularStructure(molecularStructure4);
         compound4.setType(catalogList.get(3));
         compoundList.add(compound1);
         compoundList.add(compound2);
         compoundList.add(compound3);
         compoundList.add(compound4);
         return compoundList;
     }
    
     public ArrayList<String> iniHospitalName(){
      ArrayList<String> hospitalName = new ArrayList<String>();  
      hospitalName.add("children hospital");
      hospitalName.add("municipal hospital");
      hospitalName.add("infectious hospital");
      hospitalName.add("mental hospital");
      hospitalName.add("general hospital");
      hospitalName.add("boston hospital");
      hospitalName.add("adult hospital");
      hospitalName.add("new york Hospital");
      hospitalName.add("army hospital");
      hospitalName.add("usa hospital");
      return hospitalName;
      //10
     }
     
    public ArrayList<Disease> iniDisease(ArrayList<String> drugName,ArrayList<String> diseaseName,ArrayList<String> hospitalName) throws ParseException {
        ArrayList<Disease> diseaseList = new ArrayList<Disease>();
        int diseaseId;
        int index1;
        int index2;
        int index3;
        double index4;
        int treatedTime=0;
        boolean isTreated = false;
        Random r=new Random();    
        
        for (int i = 0; i < 10000; i++) {
           Disease disease=new Disease();
           diseaseId=i;
           index1=r.nextInt(15);
           index2=r.nextInt(15);
           index3=r.nextInt(10);
           index4=Math.random();
         
           if (r.nextInt(10) % 2 == 0) {
                isTreated = true;
        }
            if(index4<0.5){
            treatedTime=r.nextInt(6);
        }
            else if(index4<0.8&&index4>=0.5){
            
            treatedTime=5+r.nextInt(11);
        }
            else if(index4>=0.8) treatedTime=15+r.nextInt(16);
           disease.setName(diseaseName.get(index1));
           disease.setDrug(drugName.get(index2));
           disease.setDiseaseId(diseaseId);
           disease.setHospital(hospitalName.get(index3));
           disease.setIsTreated(isTreated);
           disease.setTreatedTime(treatedTime);
           diseaseList.add(disease);
        }

        return diseaseList;

    }

    public ArrayList<Person> iniParentList(ArrayList<String> name, ArrayList<Disease> diseaseList) {
        ArrayList<Person> parentList = new ArrayList<Person>();
        String personalID;
      
        for (int i = 0; i < 1000; i++) {
            Person person = new Person();

            Random r = new Random();
            personalID = String.valueOf(i + 1);

            int numName = r.nextInt(1000);
            String pName = name.get(numName);
            //age
            int k = 20 + r.nextInt(45);

           
            String gender = "male";
            if (r.nextInt(10) % 2 == 0) {
                gender = "female";
            }
            boolean smoker = false;
            if (r.nextInt(10) % 2 == 0) {
                smoker = true;
            }
            boolean diabetes = false;
            if (r.nextInt(10) % 2 == 0) {
               diabetes = true;
            }
            //systolicBloodPressure
            int systolicBloodPressure=120+r.nextInt(50);
            //diseaseHistory
            double index=Math.random();
            DiseaseHistory diseaseHistory=new DiseaseHistory();
            if(index<0.4){
                 diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                 j++;
             }
            else if(index>=0.4&&index<0.7){
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
            }
            else if(index>0.7){
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++; 
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++; 
            }
            person.setPersonalID(personalID);
            person.setName(pName);
            person.setAge(k);
            person.setGender(gender);
            person.doLabel();
            person.setSmoker(smoker);
            person.setDiabetes(diabetes);
            person.setSystolicBloodPressure(systolicBloodPressure);
            person.setDiseaseHistory(diseaseHistory);
          
            parentList.add(person);
        }
        return parentList;
    }

    public ArrayList<Person> iniChildrenList(ArrayList<String> name, ArrayList<Disease> diseaseList) {
        
        ArrayList<Person> childrenList = new ArrayList<Person>();
        String personalID;
    
        for (int i = 1000; i < 1400; i++) {
            Person person = new Person();

            Random r = new Random();
            personalID = String.valueOf(i + 1);

            int numName = r.nextInt(1000);
            String pName = name.get(numName);
            //age
            int k = r.nextInt(20);

           
            String gender = "male";
            if (r.nextInt(10) % 2 == 0) {
                gender = "female";
            }
            boolean smoker = false;
            if (r.nextInt(10) % 2 == 0) {
                smoker = true;
            }
            boolean diabetes = false;
            if (r.nextInt(10) % 2 == 0) {
               diabetes = true;
            }
            //systolicBloodPressure
            int systolicBloodPressure=120+r.nextInt(50);
            //diseaseHistory
            double index=Math.random();
            DiseaseHistory diseaseHistory=new DiseaseHistory();
            if(index<0.4){
                 diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                 j++;
             }
            else if(index>=0.4&&index<0.7){
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
            }
            else if(index>0.7){
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++; 
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++; 
            }
            person.setPersonalID(personalID);
            person.setName(pName);
            person.setAge(k);
            person.setGender(gender);
            person.doLabel();
            person.setSmoker(smoker);
            person.setDiabetes(diabetes);
            person.setSystolicBloodPressure(systolicBloodPressure);
            person.setDiseaseHistory(diseaseHistory);
          
            childrenList.add(person);
        }
        return childrenList;
    }

    public ArrayList<Person> iniGrandparentList(ArrayList<String> name, ArrayList<Disease> diseaseList) {
       
        ArrayList<Person> grandparentList = new ArrayList<Person>();
        String personalID;
    
        for (int i = 1400; i < 1800; i++) {
            Person person = new Person();

            Random r = new Random();
            personalID = String.valueOf(i + 1);

            int numName = r.nextInt(1000);
            String pName = name.get(numName);
            //age
            int k = 65+r.nextInt(30);

           
            String gender = "male";
            if (r.nextInt(10) % 2 == 0) {
                gender = "female";
            }
            boolean smoker = false;
            if (r.nextInt(10) % 2 == 0) {
                smoker = true;
            }
            boolean diabetes = false;
            if (r.nextInt(10) % 2 == 0) {
               diabetes = true;
            }
            //systolicBloodPressure
            int systolicBloodPressure=120+r.nextInt(50);
            //diseaseHistory
            double index=Math.random();
            DiseaseHistory diseaseHistory=new DiseaseHistory();
            if(index<0.4){
                 diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                 j++;
             }
            else if(index>=0.4&&index<0.7){
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
            }
            else if(index>0.7){
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++;
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++; 
                diseaseHistory.getDiseaseHistory().add(diseaseList.get(j));
                j++; 
            }
            person.setPersonalID(personalID);
            person.setName(pName);
            person.setAge(k);
            person.setGender(gender);
            person.doLabel();
            person.setSmoker(smoker);
            person.setDiabetes(diabetes);
            person.setSystolicBloodPressure(systolicBloodPressure);
            person.setDiseaseHistory(diseaseHistory);
          
            grandparentList.add(person);
        }
        return grandparentList;
    }

    public ArrayList<Person> iniPerson(ArrayList<Person> parentList, ArrayList<Person> childrenList, ArrayList<Person> grandparentList) {
        ArrayList<Person> personList = new ArrayList<Person>();
        for (Person p : parentList) {
            personList.add(p);
        }
        for (Person p : childrenList) {
            personList.add(p);
        }
        for (Person p : grandparentList) {
            personList.add(p);
        }
        return personList;
    }

    public ArrayList<Family> iniFamily(ArrayList<Person> parentList, ArrayList<Person> childrenList, ArrayList<Person> grandparentList) {
        ArrayList<Family> familyList = new ArrayList<Family>();

        for (int i = 0; i < 500; i++) {
            Family family = new Family();
            Random rr = new Random();
            int size;
            size = rr.nextInt(3) + 2;
            family.setFamilySize(size);

            family.getPersonList().add(parentList.get(2 * i));
            family.getPersonList().add(parentList.get(2 * i + 1));
            //System.out.println("family id   "+size);
            for (int j = 0; j < size - 2; j++) {
                int kind = rr.nextInt(2);
                if (kind == 0) {
                    int index = rr.nextInt(childrenList.size());
                    family.getPersonList().add(childrenList.get(index));
                    //System.out.println(childrenList.get(index).getName()+" child");
                    childrenList.remove(index);
                } else {
                    int index = rr.nextInt(grandparentList.size());
                    family.getPersonList().add(grandparentList.get(index));
                    //System.out.println(grandparentList.get(index).getName()+" old");
                    grandparentList.remove(index);
                }
            }

            family.setFamilyId(String.valueOf(i));
            familyList.add(family);
            //System.out.println("done  ");

        }
        return familyList;
    }

    public ArrayList<House> iniHouse(ArrayList<Family> familyList) {
        ArrayList<House> houseList = new ArrayList<House>();
        for (int i = 0; i < 125; i++) {
            House house = new House();
            house.getFamilyList().add(familyList.get(2 * i));
            house.getFamilyList().add(familyList.get(2 * i + 1));
            house.setHouseId(String.valueOf(i));
            houseList.add(house);

        }
        return houseList;

    }

    public ArrayList<Community> iniCommunity(ArrayList<House> houseList) {
        ArrayList<Community> communityList = new ArrayList<Community>();
        for (int i = 1; i < 6; i++) {
            Community community = new Community();
            community.getHouseList().add(houseList.get(5 * i - 4));
            community.getHouseList().add(houseList.get(5 * i - 3));
            community.getHouseList().add(houseList.get(5 * i - 2));
            community.getHouseList().add(houseList.get(5 * i - 1));
            community.getHouseList().add(houseList.get(5 * i));
            community.setCommunityId(String.valueOf(i));

            communityList.add(community);
        }
        return communityList;
    }

    public City iniAll() throws ParseException {
        ArrayList<Community> communityList = this.iniCommunity(this.iniHouse(this.iniFamily(this.iniParentList(this.iniName(), this.iniDisease(this.iniDrugName(), this.iniDiseaseName(),this.iniHospitalName())), this.iniChildrenList(this.iniName(), this.iniDisease(this.iniDrugName(), this.iniDiseaseName(), this.iniHospitalName())), this.iniGrandparentList(this.iniName(), this.iniDisease(this.iniDrugName(), this.iniDiseaseName(), this.iniHospitalName())))));
        City city = new City();
        for (Community c : communityList) {
            city.getCommunityList().add(c);
        }
        return city;

    }
    

    
}
