/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Patient;

/**
 *
 * @author s
 */
public class Compound {

    private String name;
    private String Description;
    private Cata type;
    private String molecularFormula;
    private String molecularStructure;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String Description) {
        this.Description = Description;
    }

    public Cata getType() {
        return type;
    }

    public void setType(Cata type) {
        this.type = type;
    }

    public String getMolecularFormula() {
        return molecularFormula;
    }

    public void setMolecularFormula(String molecularFormula) {
        this.molecularFormula = molecularFormula;
    }

    public String getMolecularStructure() {
        return molecularStructure;
    }

    public void setMolecularStructure(String molecularStructure) {
        this.molecularStructure = molecularStructure;
    }

    
    
}
