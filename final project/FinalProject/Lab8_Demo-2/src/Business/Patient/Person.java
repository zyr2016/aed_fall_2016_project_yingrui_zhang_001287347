package Business.Patient;

import Business.Disease.Disease;
import Business.Disease.DiseaseHistory;

/**
 *
 * @author yyneu 
 */
public class Person {

    private String personalID;
    private String name;
    private int age;
    private String gender;
    private String label;
   
    private boolean smoker;
    private boolean diabetes;
    private int systolicBloodPressure;
    private DiseaseHistory diseaseHistory;
     
  
    private Point point;
    //constructer
    public Person() {
      
    }

    //according to the age set the lable
    public void doLabel() {
        if (this.age < 20) {
            label = "child";
        } else if (this.age >= 20 && this.age <= 65) {
            label = "parent";
        } else {
            label = "grandparent";
        }
    }

    //getter&setter
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

 

    public String getPersonalID() {
        return personalID;
    }

    public void setPersonalID(String personalID) {
        this.personalID = personalID;
    }
   
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public DiseaseHistory getDiseaseHistory() {
        return diseaseHistory;
    }

    public void setDiseaseHistory(DiseaseHistory diseaseHistory) {
        this.diseaseHistory = diseaseHistory;
    }

    public boolean isSmoker() {
        return smoker;
    }

    public void setSmoker(boolean smoker) {
        this.smoker = smoker;
    }

    public boolean isDiabetes() {
        return diabetes;
    }

    public void setDiabetes(boolean diabetes) {
        this.diabetes = diabetes;
    }

    public int getSystolicBloodPressure() {
        return systolicBloodPressure;
    }

    public void setSystolicBloodPressure(int systolicBloodPressure) {
        this.systolicBloodPressure = systolicBloodPressure;
    }


    public Point getPoint() {
        return point;
    }

    public void setPoint(Point point) {
        this.point = point;
    }

  


}
