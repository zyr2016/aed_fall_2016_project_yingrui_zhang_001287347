/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.AccounterRole;
import Business.Role.DoctorRole;
import Business.Role.Role;
import java.util.ArrayList;
/**
 *
 * @author s
 */
public class AccounterOrganization extends Organization{

    public AccounterOrganization() {
        super(Organization.Type.Accounter.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new AccounterRole());
        return roles;
    }
     
}