/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Organization.Organization.Type;
import java.util.ArrayList;

/**
 *
 * @author  yyneu
 */
public class OrganizationDirectory {
    
    private ArrayList<Organization> organizationList;

    public OrganizationDirectory() {
        organizationList = new ArrayList();
    }

    public ArrayList<Organization> getOrganizationList() {
        return organizationList;
    }
    
    public Organization createOrganization(Type type){
        Organization organization = null;
        if (type.getValue().equals(Type.Doctor.getValue())){
            organization = new DoctorOrganization();
            organizationList.add(organization);
        }
        else if (type.getValue().equals(Type.Lab.getValue())){
            organization = new LabOrganization();
            organizationList.add(organization);
            //////////////////////////////////////////////////////
        } else if (type.getValue().equals(Type.Accounter.getValue())){
            organization = new AccounterOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.AssistantCt.getValue())){
            organization = new AssistantCtOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.Chiefresearcher.getValue())){
            organization = new ChiefresearcherOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.Collector.getValue())){
            organization = new CollectorOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.Conductor.getValue())){
            organization = new ConductorOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.DoctorHc.getValue())){
            organization = new DoctorHcOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.Reporters.getValue())){
            organization = new ReportersOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.Suveryer.getValue())){
            organization = new SuveryerOrganization();
            organizationList.add(organization);
        }else if (type.getValue().equals(Type.Tester.getValue())){
            organization = new TesterOrganization();
            organizationList.add(organization);
        } else if (type.getValue().equals(Type.Worker.getValue())){
            organization = new WorkerOrganization();
            organizationList.add(organization);
        }
        return organization;
    }
}