/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DoctorRole;
import Business.Role.Role;
import Business.Role.TesterRole;
import java.util.ArrayList;

/**
 *
 * @author s
 */
public class TesterOrganization extends Organization{

    public TesterOrganization() {
        super(Organization.Type.Tester.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new TesterRole());
        return roles;
    }
     
}