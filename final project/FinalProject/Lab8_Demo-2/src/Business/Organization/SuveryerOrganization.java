/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Organization;

import Business.Role.DoctorRole;
import Business.Role.Role;
import Business.Role.SuveryerRole;
import java.util.ArrayList;

/**
 *
 * @author s
 */
public class SuveryerOrganization extends Organization{

    public SuveryerOrganization() {
        super(Organization.Type.Suveryer.getValue());
    }
    
    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roles = new ArrayList();
        roles.add(new SuveryerRole());
        return roles;
    }
     
}