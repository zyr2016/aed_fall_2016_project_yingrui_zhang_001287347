package Business;

import Business.Drug.DrugHistory;
import Business.Network.PatientUserAccountDirectory;
import Business.Network.Network;
import Business.Organization.Organization;
import Business.Role.Role;
import Business.Role.SystemAdminRole;
import Business.WorkQueue.AdminReport;
import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class EcoSystem extends Organization {

    private static EcoSystem business;
    private ArrayList<Network> networkList;
    private ArrayList<AdminReport> adReList;
    private ArrayList<String> messList;
    private PatientUserAccountDirectory patientUserAccountDirectory;
    private DrugHistory drugHistory;
            
    public static EcoSystem getInstance() {
        if (business == null) {
            business = new EcoSystem();
        }
        return business;
    }

    private EcoSystem() {
        super(null);
        networkList = new ArrayList<>();
        adReList=new ArrayList<>();
        messList=new ArrayList<>();
        patientUserAccountDirectory=new PatientUserAccountDirectory();
        drugHistory= new DrugHistory();
    }

    public DrugHistory getDrugHistory() {
        return drugHistory;
    }

    public void setDrugHistory(DrugHistory drugHistory) {
        this.drugHistory = drugHistory;
    }

    public PatientUserAccountDirectory getPatientUserAccountDirectory() {
        return patientUserAccountDirectory;
    }

    public ArrayList<Network> getNetworkList() {
        return networkList;
    }

    public Network createAndAddNetwork() {
        Network network = new Network();
        networkList.add(network);
        return network;
    }
    public AdminReport createAndAddAdminReport() {
        AdminReport network = new AdminReport();
        adReList.add(network);
        return network;
    }

    public static EcoSystem getBusiness() {
        return business;
    }

    public static void setBusiness(EcoSystem business) {
        EcoSystem.business = business;
    }

    public ArrayList<AdminReport> getAdReList() {
        return adReList;
    }

    public void setAdReList(ArrayList<AdminReport> adReList) {
        this.adReList = adReList;
    }

    public ArrayList<String> getMessList() {
        return messList;
    }

    public void setMessList(ArrayList<String> messList) {
        this.messList = messList;
    }
    
    
    

    @Override
    public ArrayList<Role> getSupportedRole() {
        ArrayList<Role> roleList = new ArrayList<>();
        roleList.add(new SystemAdminRole());
        return roleList;
    }

    public boolean checkIfUsernameIsUnique(String username) {

        if (!this.getUserAccountDirectory().checkIfUsernameIsUnique(username)) {
            return false;
        }
        return true;
    }
}
