/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author lenovo
 */
public class PreportDirectory {
     private ArrayList<Preport> preportList;
     
     public PreportDirectory()
     {
            preportList = new ArrayList<>();    
     }

    public ArrayList<Preport> getPreportList() {
        return preportList;
    }
     public Preport addPreport()
     {
         Preport p = new Preport();
         preportList.add(p);
         return p;
     }
     
}
