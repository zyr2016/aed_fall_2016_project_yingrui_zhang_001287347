/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Study;

/**
 *
 * @author s
 */
public class Study {

    private String studyName;
    private String studyType;
    private String studyDesign;
    private String studyPhase;
    private String recruitmentStatus;
    private String condition;
    private String intervention;
    private String officialTitle;
    private String sponsoredBy;
    private String informationProvidedBy;

    public Study(String studyName, String studyType, String studyDesign, String studyPhase, String recruitmentStatus, String condition, String intervention,   
       String officialTitle,String sponsoredBy,String informationProvidedBy){
        this.studyName = studyName;
        this.studyType = studyType;
        this.studyDesign = studyDesign;
        this.studyPhase = studyPhase;
        this.recruitmentStatus = recruitmentStatus;
        this.condition = condition;
        this.intervention = intervention;
        this.officialTitle = officialTitle;
        this.sponsoredBy = sponsoredBy;
        this.informationProvidedBy = informationProvidedBy;
    }

    public String getStudyName() {
        return studyName;
    }

    public void setStudyName(String studyName) {
        this.studyName = studyName;
    }

    public String getStudyType() {
        return studyType;
    }

    public void setStudyType(String studyType) {
        this.studyType = studyType;
    }

    public String getStudyDesign() {
        return studyDesign;
    }

    public void setStudyDesign(String studyDesign) {
        this.studyDesign = studyDesign;
    }

    public String getStudyPhase() {
        return studyPhase;
    }

    public void setStudyPhase(String studyPhase) {
        this.studyPhase = studyPhase;
    }

    public String getRecruitmentStatus() {
        return recruitmentStatus;
    }

    public void setRecruitmentStatus(String recruitmentStatus) {
        this.recruitmentStatus = recruitmentStatus;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getIntervention() {
        return intervention;
    }

    public void setIntervention(String intervention) {
        this.intervention = intervention;
    }

    public String getOfficialTitle() {
        return officialTitle;
    }

    public void setOfficialTitle(String officialTitle) {
        this.officialTitle = officialTitle;
    }

    public String getSponsoredBy() {
        return sponsoredBy;
    }

    public void setSponsoredBy(String sponsoredBy) {
        this.sponsoredBy = sponsoredBy;
    }

    public String getInformationProvidedBy() {
        return informationProvidedBy;
    }

    public void setInformationProvidedBy(String informationProvidedBy) {
        this.informationProvidedBy = informationProvidedBy;
    }

}
