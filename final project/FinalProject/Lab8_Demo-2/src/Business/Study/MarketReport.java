/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Study;

import java.util.ArrayList;

/**
 *
 * @author CAC
 */
public class MarketReport {

    private ArrayList<String> name;
    private ArrayList<String> price;

    public MarketReport() {
        name = new ArrayList<String>();
        price = new ArrayList<String>();
    }

    public ArrayList<String> getName() {
        return name;
    }

    public void setName(ArrayList<String> name) {
        this.name = name;
    }

    public ArrayList<String> getPrice() {
        return price;
    }

    public void setPrice(ArrayList<String> price) {
        this.price = price;
    }

}
