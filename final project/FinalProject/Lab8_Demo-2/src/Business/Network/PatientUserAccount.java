/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import Business.PreportDirectory;
import Business.PreportDirectory;
import Business.WorkQueue.WorkQueue;
import javax.swing.ImageIcon;

/**
 *
 * @author lenovo
 */
public class PatientUserAccount {
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private String age;
    private WorkQueue workQueue;
    private String gender;
    private  ImageIcon image;
    private PreportDirectory preportDirectory;
    
    public PatientUserAccount() {
        workQueue = new WorkQueue();
        preportDirectory= new PreportDirectory();
    }

    public PreportDirectory getPreportDirectory() {
        return preportDirectory;
    }
      
    public ImageIcon getImage() {
        return image;
    }

    public void setImage(ImageIcon image) {
        this.image = image;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

  
    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

   
    public WorkQueue getWorkQueue() {
        return workQueue;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
    
    
     @Override
    public String toString() {
        return username;
    }
    
}
