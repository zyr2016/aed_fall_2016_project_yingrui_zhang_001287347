
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Network;

import java.util.ArrayList;
import javax.swing.ImageIcon;

/**
 *
 * @author lenovo
 */
public class PatientUserAccountDirectory {
    private ArrayList<PatientUserAccount> patientUserAccountList;
    
     public PatientUserAccountDirectory() {
        patientUserAccountList = new ArrayList<PatientUserAccount>();
    }

    public ArrayList<PatientUserAccount> getPatientUserAccountList() {
        return patientUserAccountList;
    }

  public PatientUserAccount authenticateUser(String username, String password){
        for (PatientUserAccount pua : patientUserAccountList)
            if (pua.getUsername().equals(username) && pua.getPassword().equals(password)){
                return pua;
            }
        return null;
    }
    
    public PatientUserAccount createUserAccount(String username, String password,String firstName,String lastName,String gender,String age,ImageIcon imageIcon){
        PatientUserAccount userAccount = new PatientUserAccount();
        userAccount.setUsername(username);
        userAccount.setFirstName(firstName);
        userAccount.setLastName(lastName);
        userAccount.setPassword(password);
        userAccount.setAge(age);
        userAccount.setGender(gender);
        userAccount.setImage(imageIcon);
        patientUserAccountList.add(userAccount);
        return userAccount;
    }

   public boolean checkIfUsernameIsUnique(String username){
       for (PatientUserAccount ua : patientUserAccountList){
            if (ua.getUsername().equals(username))
                return false;
        }
        return true;
    }
       
}
