/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Disease;

import Business.Patient.Person;
import java.util.ArrayList;

/**
 *
 * @author s
 */
public class Disease {
   private String name;
   private int diseaseId;
   private String drug;
   private String hospital;
   private boolean isTreated;
   private int treatedTime; 

   private Person person;
    
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDiseaseId() {
        return diseaseId;
    }

    public void setDiseaseId(int diseaseId) {
        this.diseaseId = diseaseId;
    }

    public String getDrug() {
        return drug;
    }

    public void setDrug(String drug) {
        this.drug = drug;
    }

    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    public String getHospital() {
        return hospital;
    }

    public void setHospital(String hospital) {
        this.hospital = hospital;
    }

    public boolean isIsTreated() {
        return isTreated;
    }

    public void setIsTreated(boolean isTreated) {
        this.isTreated = isTreated;
    }

    public int getTreatedTime() {
        return treatedTime;
    }

    public void setTreatedTime(int treatedTime) {
        this.treatedTime = treatedTime;
    }
   
}
