/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.Disease;

import java.util.ArrayList;

/**
 *
 * @author yyneu
 */
public class DiseaseHistory {
    private ArrayList<Disease> diseaseHistory;
    private static int count=0;
    private int id;

    public DiseaseHistory() {
        count++;
        id=count;
        diseaseHistory=new ArrayList<Disease>();
    }
    

    public ArrayList<Disease> getDiseaseHistory() {
        return diseaseHistory;
    }
   
    public void setDiseaseHistory(ArrayList<Disease> diseaseHistory) {
        this.diseaseHistory = diseaseHistory;
    }

   
}
