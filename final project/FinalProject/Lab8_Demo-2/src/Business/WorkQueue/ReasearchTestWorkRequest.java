/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business.WorkQueue;

import Business.Drug.Drug;
import Business.Study.MarketReport;
import Business.Study.Study;

/**
 *
 * @author CAC
 */
public class ReasearchTestWorkRequest extends WorkRequest {

    private String testResult;
    private MarketReport mktr;
    private Study study;
    private String to;
    private Drug dg;
    
    
    
    public ReasearchTestWorkRequest(){
    dg=new Drug();
    dg.setDrugName("HEIHEIHEI");
    dg.setFormula("NH3-SO4-IO");
    }
    public MarketReport getMktr() {
        return mktr;
    }

    public void setMktr(MarketReport mktr) {
        this.mktr = mktr;
    }

    public String getTestResult() {
        return testResult;
    }

    public void setTestResult(String testResult) {
        this.testResult = testResult;
    }

    public Study getStudy() {
        return study;
    }

    public void setStudy(Study study) {
        this.study = study;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Drug getDg() {
        return dg;
    }

    public void setDg(Drug dg) {
        this.dg = dg;
    }
    
    
}
