/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package userinterface.Marketing;

import Business.Disease.Disease;
import Business.Patient.City;
import Business.Patient.Community;
import Business.Patient.Family;
import Business.Patient.House;
import Business.Patient.InitialData;
import Business.Patient.Person;
import Business.Patient.Point;
import Business.Patient.Report;
import java.awt.CardLayout;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.JPanel;

/**
 *
 * @author yyneu
 */
public class CountryReportJPanel extends javax.swing.JPanel {
 private JPanel userProcessContainer;
int pointer=1;
City city;
String labelName;
int result;
String condition;
ArrayList<Person> personList;
String mostUse;

HashMap<String,Integer> mostHashmap=new HashMap<String,Integer>();
ArrayList<String> communityRiskList=new ArrayList<String>();
       int agePoint;
       int smokePoint;
       int systolicPoint;
       int diabetesPoint;
       int diseaseHistoryPoint;
       int totalPoint;
       
       String risk;
       ArrayList<String> diseaseName;
       ArrayList<String> drugName;
       ArrayList<String> hospitalName;
       ArrayList<Disease> diseaseList;
       ArrayList<Person> iniParentList;
       ArrayList<Person> iniChildrenList;
       ArrayList<Person> iniGrandparentList;
       ArrayList<Person> iniPersonList;
       ArrayList<Family> familyList;
       ArrayList<House> houseList;
       ArrayList<Community> communityList;
    /**
     * Creates new form CityReportJPanel
     */
 

    public CountryReportJPanel(JPanel userProcessContainer) throws ParseException {
        initComponents();
    this.userProcessContainer=userProcessContainer;
  
      InitialData ww=new InitialData();
       city = ww.iniAll(); 
    
       
       
       diseaseName=ww.iniDiseaseName();
       drugName=ww.iniDrugName();
       hospitalName=ww.iniHospitalName();
       diseaseList=ww.iniDisease(drugName, diseaseName, hospitalName);
       iniParentList = ww.iniParentList(ww.iniName(),diseaseList);
       iniChildrenList = ww.iniChildrenList(ww.iniName(),diseaseList);
       iniGrandparentList = ww.iniGrandparentList(ww.iniName(),diseaseList);
      
       
         ArrayList<Person> parentList = new ArrayList<Person>();
         for(Person p:iniParentList){
             Point point = new Point();
             agePoint=point.agePoint(p.getAge(), p.getGender());
             smokePoint=point.smokePoint(p.isSmoker(), p.getAge(), p.getGender());
             systolicPoint=point.systolicPoint(p.getSystolicBloodPressure(), p.getGender());
             diabetesPoint=point.diabetesPoint(p.isDiabetes(), p.getAge());
             diseaseHistoryPoint=point.diseaseHistoryPoint(p.getDiseaseHistory(), p.getAge());
             totalPoint=point.totalPoint(agePoint, smokePoint, systolicPoint, diabetesPoint, diseaseHistoryPoint);
             risk=point.report(totalPoint);
             point.setAgePoint(agePoint);
             point.setSmokePoint(smokePoint);
             point.setSystolicPoint(systolicPoint);
             point.setDiabetesPoint(diabetesPoint);
             point.setDiseaseHistoryPoint(diseaseHistoryPoint);
             point.setTotalPoint(totalPoint);
             point.setRisk(risk);//calculate different person's point
             p.setPoint(point);
             parentList.add(p);
         }
        iniParentList=parentList;
        
         ArrayList<Person> childrenList = new ArrayList<Person>();
         for(Person p:iniChildrenList){
             Point point = new Point();
             agePoint=point.agePoint(p.getAge(), p.getGender());
             smokePoint=point.smokePoint(p.isSmoker(), p.getAge(), p.getGender());
             systolicPoint=point.systolicPoint(p.getSystolicBloodPressure(), p.getGender());
             diabetesPoint=point.diabetesPoint(p.isDiabetes(), p.getAge());
             diseaseHistoryPoint=point.diseaseHistoryPoint(p.getDiseaseHistory(), p.getAge());
             totalPoint=point.totalPoint(agePoint, smokePoint, systolicPoint, diabetesPoint, diseaseHistoryPoint);
             risk=point.report(totalPoint);
             point.setAgePoint(agePoint);
             point.setSmokePoint(smokePoint);
             point.setSystolicPoint(systolicPoint);
             point.setDiabetesPoint(diabetesPoint);
             point.setDiseaseHistoryPoint(diseaseHistoryPoint);
             point.setTotalPoint(totalPoint);
             point.setRisk(risk);//calculate different person's point
             p.setPoint(point);
             childrenList.add(p);
         }
        iniChildrenList=childrenList;
        
        
         ArrayList<Person> grandparentList = new ArrayList<Person>();
         for(Person p:iniGrandparentList){
             Point point = new Point();
             agePoint=point.agePoint(p.getAge(), p.getGender());
             smokePoint=point.smokePoint(p.isSmoker(), p.getAge(), p.getGender());
             systolicPoint=point.systolicPoint(p.getSystolicBloodPressure(), p.getGender());
             diabetesPoint=point.diabetesPoint(p.isDiabetes(), p.getAge());
             diseaseHistoryPoint=point.diseaseHistoryPoint(p.getDiseaseHistory(), p.getAge());
             totalPoint=point.totalPoint(agePoint, smokePoint, systolicPoint, diabetesPoint, diseaseHistoryPoint);
             risk=point.report(totalPoint);
             point.setAgePoint(agePoint);
             point.setSmokePoint(smokePoint);
             point.setSystolicPoint(systolicPoint);
             point.setDiabetesPoint(diabetesPoint);
             point.setDiseaseHistoryPoint(diseaseHistoryPoint);
             point.setTotalPoint(totalPoint);
             point.setRisk(risk);//calculate different person's point
             p.setPoint(point);
             grandparentList.add(p);
         }
         iniGrandparentList=grandparentList;
        
         iniPersonList = ww.iniPerson(iniParentList, iniChildrenList, iniGrandparentList);
         personList=iniPersonList;
         familyList = ww.iniFamily(iniParentList, iniChildrenList, iniGrandparentList);
         houseList = ww.iniHouse(familyList);
         communityList = ww.iniCommunity(houseList);
         city.setCommunityList(communityList);
           
        txt1.setText(String.valueOf(communityList.size()));
        txt2.setText(String.valueOf(houseList.size()));
        txt3.setText(String.valueOf(familyList.size()));
        txt4.setText(String.valueOf(personList.size()));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        reportBtnGroup = new javax.swing.ButtonGroup();
        btn1 = new javax.swing.JRadioButton();
        btn2 = new javax.swing.JRadioButton();
        btn3 = new javax.swing.JRadioButton();
        btn4 = new javax.swing.JRadioButton();
        btn5 = new javax.swing.JRadioButton();
        btn6 = new javax.swing.JRadioButton();
        btn7 = new javax.swing.JRadioButton();
        btn8 = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        btnView = new javax.swing.JButton();
        btnBack = new javax.swing.JButton();
        txt1 = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt2 = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt3 = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt4 = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();

        reportBtnGroup.add(btn1);
        btn1.setSelected(true);
        btn1.setText("How many people have high risk health condition in this country?");
        btn1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn1ActionPerformed(evt);
            }
        });

        reportBtnGroup.add(btn2);
        btn2.setText("How many women in high risk of health condition compared to men");
        btn2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn2ActionPerformed(evt);
            }
        });

        reportBtnGroup.add(btn3);
        btn3.setText("What is average health status of each community");
        btn3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn3ActionPerformed(evt);
            }
        });

        reportBtnGroup.add(btn4);
        btn4.setText("How many familires' people both in low risk of health condition");
        btn4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn4ActionPerformed(evt);
            }
        });

        reportBtnGroup.add(btn5);
        btn5.setText("The must common health status of people");
        btn5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn5ActionPerformed(evt);
            }
        });

        reportBtnGroup.add(btn6);
        btn6.setText("How many people above 30 years old have high risk to be ill");
        btn6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn6ActionPerformed(evt);
            }
        });

        reportBtnGroup.add(btn7);
        btn7.setText("What is the most drug has been used");
        btn7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn7ActionPerformed(evt);
            }
        });

        reportBtnGroup.add(btn8);
        btn8.setText("What is the most disease people have ");
        btn8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn8ActionPerformed(evt);
            }
        });

        jLabel1.setText("Select the report you want to view");

        btnView.setText("view");
        btnView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnViewActionPerformed(evt);
            }
        });

        btnBack.setText("Back");
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        txt1.setEditable(false);

        jLabel2.setText("There are :");

        jLabel3.setText("communities");

        txt2.setEditable(false);

        jLabel4.setText("families");

        txt3.setEditable(false);

        jLabel5.setText("houses");

        txt4.setEditable(false);

        jLabel6.setText("people");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn2)
                            .addComponent(btn3)
                            .addComponent(btn4)
                            .addComponent(btn1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn5)
                            .addComponent(btn8)
                            .addComponent(btn7)
                            .addComponent(btn6)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(385, 385, 385)
                        .addComponent(jLabel1))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(330, 330, 330)
                                .addComponent(btnView))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt4, javax.swing.GroupLayout.DEFAULT_SIZE, 90, Short.MAX_VALUE)
                                    .addComponent(txt3)
                                    .addComponent(txt2)
                                    .addComponent(txt1))
                                .addGap(18, 18, 18)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4)
                                    .addComponent(jLabel5)
                                    .addComponent(jLabel6))))))
                .addContainerGap(175, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(18, 18, 18)
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn1)
                            .addComponent(btn5))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn6)
                            .addComponent(btn2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btn7, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btn3))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn4)
                            .addComponent(btn8))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2)
                            .addComponent(txt1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel5))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btnView)
                    .addComponent(btnBack))
                .addContainerGap())
        );
    }// </editor-fold>//GEN-END:initComponents

    private void btnViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnViewActionPerformed
        System.out.println(city.getCommunityList().size());
        System.out.println(communityList.size());
        System.out.println(houseList.size());
        System.out.println(familyList.size());
        System.out.println(personList.size());
        if(pointer==3){
        CommunityRiskJPanel panel = new CommunityRiskJPanel(userProcessContainer,communityRiskList,labelName);
         
        userProcessContainer.add("CommunityRiskJPanel",panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer); 
       }
       else if(pointer==5){
        CommonRiskJPanel panel = new CommonRiskJPanel(userProcessContainer,condition,labelName);
        userProcessContainer.add("CommonRiskJPanel",panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer); 
       }
       else if(pointer==7||pointer==8){
         ShowMostJPanel panel = new ShowMostJPanel(userProcessContainer,mostUse,labelName);
        userProcessContainer.add("ShowMostJPanel",panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer); 
        }
        else {
        ShowReportJPanel panel = new ShowReportJPanel(userProcessContainer,result,labelName);
        userProcessContainer.add("ShowReportJPanel",panel);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.next(userProcessContainer);
               }
    }//GEN-LAST:event_btnViewActionPerformed

    private void btn1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn1ActionPerformed
        pointer=1;// TODO add your handling code here:
        labelName="How many people have high risk health condition in this city?";
        Report r=new Report();
        result=r.peopleHighRiskCity(personList);
    }//GEN-LAST:event_btn1ActionPerformed

    private void btn2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn2ActionPerformed
        pointer=2;// TODO add your handling code here:
        labelName="How many women in high risk of health condition compared to men";
        Report r=new Report();
        result=r.femaleToMaleHighRisk(personList);
    }//GEN-LAST:event_btn2ActionPerformed

    private void btn3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn3ActionPerformed
        pointer=3;// TODO add your handling code here:
        labelName="What is average health status of each community";
        Report r=new Report();
        communityRiskList=r.averageHealthStatusPerCommunity(city);
    }//GEN-LAST:event_btn3ActionPerformed

    private void btn4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn4ActionPerformed
        pointer=4;// TODO add your handling code here:
        labelName="How many familires' people both in low risk of health condition";
        Report r=new Report();
        result=r.familyLowRiskNumber(familyList);
    }//GEN-LAST:event_btn4ActionPerformed

    private void btn5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn5ActionPerformed
        pointer=5;// TODO add your handling code here:
        labelName="The must common health status of people";
        Report r=new Report();
        condition=r.peopleCommonStatus(personList);
    }//GEN-LAST:event_btn5ActionPerformed

    private void btn6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn6ActionPerformed
        pointer=6;// TODO add your handling code here:
        labelName="How many people above 30 years old have high risk to be ill";
        Report r=new Report();
        result=r.peopleHighRiskThrityYearsOld(personList);
    }//GEN-LAST:event_btn6ActionPerformed

    private void btn7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn7ActionPerformed
        pointer=7;// TODO add your handling code here:
        labelName="What is the most drug has been used";
        Report r = new Report();
        mostUse=r.TopUsedDrug(personList);
    }//GEN-LAST:event_btn7ActionPerformed

    private void btn8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn8ActionPerformed
        pointer=8;// TODO add your handling code here:
        labelName="What is the most disease people have";
        Report r= new Report();
        mostUse=r.TopDiseasePeopleHave(personList);
    }//GEN-LAST:event_btn8ActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
  userProcessContainer.remove(this);
        CardLayout layout = (CardLayout) userProcessContainer.getLayout();
        layout.previous(userProcessContainer);        // TODO add your handling code here:
    }//GEN-LAST:event_btnBackActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton btn1;
    private javax.swing.JRadioButton btn2;
    private javax.swing.JRadioButton btn3;
    private javax.swing.JRadioButton btn4;
    private javax.swing.JRadioButton btn5;
    private javax.swing.JRadioButton btn6;
    private javax.swing.JRadioButton btn7;
    private javax.swing.JRadioButton btn8;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnView;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.ButtonGroup reportBtnGroup;
    private javax.swing.JTextField txt1;
    private javax.swing.JTextField txt2;
    private javax.swing.JTextField txt3;
    private javax.swing.JTextField txt4;
    // End of variables declaration//GEN-END:variables
}
