/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Test;

import java.awt.Font;
import java.util.ArrayList;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.CategoryDataset;
import org.jfree.data.category.DefaultCategoryDataset;

public class BarChart {

    ChartPanel frame1;
    private String name;
    private ArrayList<String> number;

    public BarChart(ArrayList<String> number11, String name) {
        this.name = name;
        // this.number=number;
        this.number = number11;

//       number.add("111");
//        number.add("111");
//        number.add("111");
//        
//         number.add("111");
//        number.add("111");
//        number.add("111");
//        
//        
//         number.add("111");
//        number.add("111");
//        number.add("111");
//        
//         number.add("111");
//        number.add("111");
//        number.add("111");
//        
//         number.add("111");
//        number.add("111");
//        number.add("111");
//        
        System.out.println("size is nimei de" + number.size());

        for (String a : this.number) {
            System.out.println(a + "is in the arr");
        }

        CategoryDataset dataset = getDataSet();
        JFreeChart chart = ChartFactory.createBarChart3D(
                "Drugs", // 图表标题  
                "Test group", // 目录轴的显示标签  
                "per ml", // 数值轴的显示标签  
                dataset, // 数据集  
                PlotOrientation.VERTICAL, // 图表方向：水平、垂直  
                true, // 是否显示图例(对于简单的柱状图必须是false)  
                false, // 是否生成工具  
                false // 是否生成URL链接  
        );

        //从这里开始  
        CategoryPlot plot = chart.getCategoryPlot();//获取图表区域对象  
        CategoryAxis domainAxis = plot.getDomainAxis();         //水平底部列表  
        domainAxis.setLabelFont(new Font("黑体", Font.BOLD, 14));         //水平底部标题  
        domainAxis.setTickLabelFont(new Font("宋体", Font.BOLD, 12));  //垂直标题  
        ValueAxis rangeAxis = plot.getRangeAxis();//获取柱状  
        rangeAxis.setLabelFont(new Font("黑体", Font.BOLD, 15));
        chart.getLegend().setItemFont(new Font("黑体", Font.BOLD, 15));
        chart.getTitle().setFont(new Font("宋体", Font.BOLD, 20));//设置标题字体  

        //到这里结束，虽然代码有点多，但只为一个目的，解决汉字乱码问题  
        frame1 = new ChartPanel(chart, true);        //这里也可以用chartFrame,可以直接生成一个独立的Frame  

    }

    private CategoryDataset getDataSet() {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();
        dataset.addValue(Integer.valueOf(number.get(0)), "cost", this.name + "1");
        dataset.addValue(Integer.valueOf(number.get(1)), "efficiency", this.name + "1");
        dataset.addValue(Integer.valueOf(number.get(2)), "cost", this.name + "1");
        dataset.addValue(Integer.valueOf(number.get(3)), "safty", this.name + "2");
        dataset.addValue(Integer.valueOf(number.get(4)), "efficiency", this.name + "2");
        dataset.addValue(Integer.valueOf(number.get(5)), "cost", this.name + "2");
        dataset.addValue(Integer.valueOf(number.get(6)), "safty", this.name + "3");
        dataset.addValue(Integer.valueOf(number.get(7)), "efficiency", this.name + "3");
        dataset.addValue(Integer.valueOf(number.get(8)), "cost", this.name + "3");
        dataset.addValue(Integer.valueOf(number.get(9)), "safty", this.name + "4");
        dataset.addValue(Integer.valueOf(number.get(10)), "efficiency", this.name + "4");
        dataset.addValue(Integer.valueOf(number.get(11)), "cost", this.name + "4");
        dataset.addValue(Integer.valueOf(number.get(12)), "safty", this.name + "5");
        dataset.addValue(Integer.valueOf(number.get(13)), "efficiency", this.name + "5");
        dataset.addValue(Integer.valueOf(number.get(14)), "cost", this.name + "5");
        return dataset;
    }

    public ChartPanel getChartPanel() {
        return frame1;

    }
}
